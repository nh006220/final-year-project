# Final year project

Sam Pink Final year project. Machine learning model running on the JASMIN supercomputer. the aim of this project is to look at a public booking data set and produce a model that is able to predict which bookings will be canceled in the future  


# Project report objectives
1. Literature review: scientific awareness and the understanding of background knowledge (20%)
2. Understanding of project aims and objectives (5%)
3. Methodology (or system design) (10%)
4. Implementations: completeness and quality of systems/experiments (15%)
5. Effective use of technology and evidence of technical competence (5%)
6. Results (or achievements of the objectives) (10%)
7. Discussion and analysis (or testing and validation) (10%)
8. Originality, contributions, and initiative (10%)
9. Organization and presentation (10%)
10. Reflection (10%)


# Deliverables
1. Project management (5%)
2. Project Demo (35%)
3. Project report (60%)
